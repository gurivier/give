# GIVE - Graph Image to Vertices and Edges

GIVE retrieves a graph from an image and gives vertices' coordinates and edges topology. The color of the image background, the vertices, and the edges must fit in ranges of values. The source code is written in ANSI C and uses the GDK library.
