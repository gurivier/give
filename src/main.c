/**
 * This program retrieves a graph from an image and gives
 * vertices' coordinates and edges topology. The color of
 * the image background, the vertices, and the edges must
 * fit in ranges of values.
 *
 * AUTHOR: Guillaume Riviere (C) 2021
 * FILE: main.c
 * VERSION: 0.07
 */

#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include "give.h"

GtkBuilder *builder = NULL ;

void display_in_window (t_image data) ;

/**
 * Main function
 */
int main (int argc, char *argv[]) {
  char *imgfile ;
  t_image image ;
  t_params params ;

  gtk_init (&argc, &argv);
  
  /** Check args count */
  if (argc != 14) {
    fprintf (stderr, "Usage: give <imgfile> <NODE COLOR MIN> <NODE COLOR MAX> <EDGE COLOR MIN> <EDGE COLOR MAX>\n");
    fprintf (stderr, " with COLORS given as: R G B\n");
    fprintf (stderr, "Example:\n");
    fprintf (stderr, " give test_1980x1080.png  0 200 0  50 255 50  200 0 0  255 50 50\n");
    exit (1) ;
  }
  
  /** Get args */
  imgfile = argv[1] ;

  /** Paramaters */
  params.node_rmin = atoi(argv[2]) ;
  params.node_gmin = atoi(argv[3]) ;
  params.node_bmin = atoi(argv[4]) ;

  params.node_rmax = atoi(argv[5]) ;
  params.node_gmax = atoi(argv[6]) ;
  params.node_bmax = atoi(argv[7]) ;

  params.edge_rmin = atoi(argv[8]) ;
  params.edge_gmin = atoi(argv[9]) ;
  params.edge_bmin = atoi(argv[10]) ;

  params.edge_rmax = atoi(argv[11]) ;
  params.edge_gmax = atoi(argv[12]) ;
  params.edge_bmax = atoi(argv[13]) ;
  
  /** Treatment */
  image = give_open_image (imgfile) ;
  give_extract (&image, params) ;

  /** Results log */
  give_log_write ("log.csv", image) ;

  /** Show results in a window */
  display_in_window (image) ;

  /** Free resources */
  give_free_image (image) ;
  
  return EXIT_SUCCESS ;
}

void display_in_window (t_image data) {
  GtkWindow  *mainwindow = NULL ;
  GtkImage   *image_original = NULL ;
  GtkImage   *image_treated = NULL ;
  GdkPixbuf *gdk_pixbuf_treated ;
  int i, node1, node2 ;  
  char txt[100] ;
  GtkTextBuffer *buffer_nodes, *buffer_edges ;
  GtkTextView *textview_nodes, *textview_edges ;
  GtkLabel *label_filename ;
  
  /** Create widgets with GTK builder */
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, "give.glade", NULL);
  gtk_builder_connect_signals (builder, NULL);

  mainwindow = GTK_WINDOW (gtk_builder_get_object (builder, "mainwindow"));
  image_original = GTK_IMAGE (gtk_builder_get_object (builder, "image_original"));
  image_treated = GTK_IMAGE (gtk_builder_get_object (builder, "image_treated"));
  label_filename = GTK_LABEL (gtk_builder_get_object (builder, "label_filename"));
  
  gtk_widget_set_events (GTK_WIDGET(gtk_builder_get_object (builder, "viewport_original")), GDK_POINTER_MOTION_MASK | GDK_LEAVE_NOTIFY_MASK);
  gtk_widget_set_events (GTK_WIDGET(gtk_builder_get_object (builder, "viewport_treated")), GDK_POINTER_MOTION_MASK | GDK_LEAVE_NOTIFY_MASK);
  
  gtk_window_set_default_size (mainwindow, 800, 600);

  gtk_label_set_text (GTK_LABEL (label_filename), data.filename);
  
  /** Draw treated pixbuf */
  
  gdk_pixbuf_treated = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, data.w, data.h);

  draw_pixbuf_from_matrix (gdk_pixbuf_treated, data) ;
  
  /** Display pixbuf in GtkImage widgets */
  
  gtk_image_set_from_pixbuf(image_original, data.gdk_pixbuf) ;
  gtk_widget_queue_draw(GTK_WIDGET(image_original));

  gtk_image_set_from_pixbuf(image_treated, gdk_pixbuf_treated) ;
  gtk_widget_queue_draw(GTK_WIDGET(image_treated));

  /** Display nodes coordinates */
  
  textview_nodes = GTK_TEXT_VIEW (gtk_builder_get_object (builder, "textview_nodes"));
  buffer_nodes = gtk_text_view_get_buffer (textview_nodes) ;
  
  for (i=0 ; i < data.nodes_centers.count ; i++) {
    sprintf (txt, "[%d] = (%d, %d)\n", i+1, data.nodes_centers.point[i].x-1, data.nodes_centers.point[i].y-1) ;

    gtk_text_buffer_insert_at_cursor (buffer_nodes, txt, strlen(txt));    
  }

  /** Display edges coordinates */
  
  textview_edges = GTK_TEXT_VIEW (gtk_builder_get_object (builder, "textview_edges"));
  buffer_edges = gtk_text_view_get_buffer (textview_edges) ;

  for (i=0 ; i < data.edges.count ; i++) {
    node1 = data.edges.edge[i].start ;
    node2 = data.edges.edge[i].end ;
    sprintf (txt, "[%d] = (%d, %d) -- [%d] = (%d, %d)\n", node1+1, data.nodes_centers.point[node1].x-1, data.nodes_centers.point[node1].y-1, node2+1, data.nodes_centers.point[node2].x-1, data.nodes_centers.point[node2].y-1) ;

    gtk_text_buffer_insert_at_cursor (buffer_edges, txt, strlen(txt));    
  }

  /** Show main window */
  gtk_widget_show_all (GTK_WIDGET(mainwindow));
  
  /** Launch GTK idle */
  gtk_main ();

  /** Free resources */
  g_object_unref (G_OBJECT (builder));
  g_object_unref (G_OBJECT (gdk_pixbuf_treated));
}

