/**
 * This module helps measuring and showing time.
 *
 * AUTHOR: Guillaume Riviere (C) 2021
 * FILE: main.c
 * VERSION: 0.07
 */

#ifndef __EXECTIME_H
#define __EXECTIME_H

#ifdef __unix

#include <time.h>
typedef struct timespec t_exectime ;

#elif _WIN32

#include <windows.h>
typedef SYSTEMTIME t_exectime ;

#endif  

t_exectime exectime_gettime () ;

double exectime_showtime (t_exectime time_start) ;

#endif /* __EXECTIME_H */
