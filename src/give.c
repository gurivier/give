/**
 * This module retrieves a graph from an image and gives
 * vertices' coordinates and edges topology. The color of
 * the image background, the vertices, and the edges must
 * fit in ranges of values.
 *
 * AUTHOR: Guillaume Riviere (C) 2021
 * FILE: main.c
 * VERSION: 0.07
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <gtk/gtk.h>

#include "give.h"
#include "exectime.h"

void explore_node_it(int *m, int W, int H, int x, int y, t_points *nodes_centers, t_points *edges_start_points) ;
void add_node(t_integers *connex_nodes, int val) ;
void add_edge(t_edges *edges, int node1, int node2) ;
void explore_edge_it(int *m, int W, int H, int x, int y, t_edges *edges) ;
void color_matrix_to_image (guchar *pr, guchar *pg, guchar *pb, int val) ;

t_pointss unexplored;

t_image give_open_image (char *filename) {
  t_image data ;
  GdkPixbuf *gdk_pixbuf ;
  int w, h, W, H ;
  int *matrix ;
  int x, y ;
  int *p, *py ;
  
  /** Processing start time */
  
  printf ("=== START PROCESSING ===\n") ;

  data.time_start = exectime_gettime ();
  
  printf ("[Loading image file...]\n") ;
  
  /** Create a GdkPixbuf from the image file */
  gdk_pixbuf = gdk_pixbuf_new_from_file (filename, NULL);

  if (gdk_pixbuf == NULL) {
    fprintf (stderr, "Error: cannot open image file \"%s\"\n", filename) ;
    exit (1) ;
  }
  
  w = gdk_pixbuf_get_width (gdk_pixbuf) ;
  h = gdk_pixbuf_get_height (gdk_pixbuf) ;
  W = w+2 ;
  H = h+2 ;
  
  /** Display processing execution time */
  data.execution_time_ms = exectime_showtime (data.time_start) ;

  printf ("[Allocate memory...]\n") ;
  
  printf ("imgfile=\"%s\"\n", filename) ;
  printf ("w=%d h=%d\n", w, h) ;
  
  /** Allocate matrix */
  matrix = malloc (W*H * sizeof(int)) ;

  if (matrix == NULL) {
    perror ("Error: memory allocation") ;
    exit (1);
  }

  p = matrix ; /** first row */
  py = matrix+((H-1)*W) ; /** last row */
  for (x=0 ; x<W ; x++) {
    *(p+x) = C_EMPTY ;
    *(py+x) = C_EMPTY ;
  }

  p = matrix ;
  for (y=0 ; y<H ; y++) {
    *p = C_EMPTY ; /** first col */
    *(p+W-1) = C_EMPTY ; /** last col */
    p += W ;
  }

  /** Display processing execution time */
  data.execution_time_ms = exectime_showtime (data.time_start) ;

  /** Init vectors */
  data.nodes_centers.count = 0 ;
  data.edges.count = 0 ;

  data.filename = filename ;
  data.gdk_pixbuf = gdk_pixbuf ;
  data.w = w ;
  data.h = h ;
  data.matrix = matrix ;
  data.W = W ;
  data.H = H ;
  
  return data ;
}

void give_free_image (t_image data) {

  /** Free memory */
  free (data.matrix) ;
  
  g_object_unref (G_OBJECT (data.gdk_pixbuf));
  
}

void give_extract (t_image *data, t_params params) {
  int *px, *py ;
  t_points edges_start_points ;
  guchar *buffer ;
  int w = data->w, h = data->h ;
  int *matrix = data->matrix ;
  int W = data->W, H = data->H ;
  guchar r, g, b ;
  int x, y ;
  int i, node1, node2 ;  

  unsigned char
    node_rmin = params.node_rmin, node_rmax = params.node_rmax,
    node_gmin = params.node_gmin, node_gmax = params.node_gmax,
    node_bmin = params.node_bmin, node_bmax = params.node_bmax,
    edge_rmin = params.edge_rmin, edge_rmax = params.edge_rmax,
    edge_gmin = params.edge_gmin, edge_gmax = params.edge_gmax,
    edge_bmin = params.edge_bmin, edge_bmax = params.edge_bmax ;
  
  /** Init vectors */
  edges_start_points.count = 0 ;

  printf ("[Analyzing image...]\n") ;
  
  /** Fill unexplored matrix from image values */

  buffer = gdk_pixbuf_get_pixels(data->gdk_pixbuf) ;
                
  if (gdk_pixbuf_get_has_alpha(data->gdk_pixbuf)) {
    /* printf ("RGBA\n") ; */

    py = matrix + W ;
    for (y=1 ; y<=h ; y++) {
      for (x=1 ; x<=w ; x++) {
        px = py + x ;
        
        r = *(buffer++) ;
        g = *(buffer++) ;
        b = *(buffer++) ;
        buffer++ ;
  
        if (r >= node_rmin && r <= node_rmax
            && g >= node_gmin && g <= node_gmax
            && b >= node_bmin && b <= node_bmax) {
          *px = C_NODE ;
        }
        else if (r >= edge_rmin && r <= edge_rmax
                 && g >= edge_gmin && g <= edge_gmax
                 && b >= edge_bmin && b <= edge_bmax) {
          *px = C_EDGE ;
        }
        else {
          *px = C_EMPTY ;
        }
      }
      py += W ;
    }
  }
  else {
    /* printf ("RGB\n") ; */
    
    py = matrix + W ;
    for (y=1 ; y<=h ; y++) {
      for (x=1 ; x<=w ; x++) {
        px = py + x ;
        
        r = *(buffer++) ;
        g = *(buffer++) ;
        b = *(buffer++) ;
  
        if (r >= node_rmin && r <= node_rmax
            && g >= node_gmin && g <= node_gmax
            && b >= node_bmin && b <= node_bmax) {
          *px = C_NODE ;
        }
        else if (r >= edge_rmin && r <= edge_rmax
                 && g >= edge_gmin && g <= edge_gmax
                 && b >= edge_bmin && b <= edge_bmax) {
          *px = C_EDGE ;
        }
        else {
          *px = C_EMPTY ;
        }
      }
      py += W ;
    }
  }
  
  /* give_matrix_write (matrix, w, h, "matrix3.txt"); */

  /** Display processing execution time */
  data->execution_time_ms = exectime_showtime (data->time_start) ;

  printf ("[Nodes detection...]\n") ;
  
  /** Explore the matrix and detect nodes, and get pixels of edges connex points */
  py = matrix + W ;
  for (y=1 ; y<=h ; y++) {
    for (x=1 ; x<=w ; x++) {
      px = py + x ;
      if (*px == C_NODE) {
  
        explore_node_it(matrix, W, H, x, y, &data->nodes_centers, &edges_start_points) ;
                
        /* printf("DONE\n"); */
      }
    }
    py += W ;
  }

  /** Display the number of nodes */
  printf ("Nodes: %d\n", data->nodes_centers.count) ;
  
  /** Display nodes coordinates */
  for (i=0 ; i < data->nodes_centers.count ; i++) {
    printf ("(%d, %d)\n", data->nodes_centers.point[i].x-1, data->nodes_centers.point[i].y-1) ;
  }

  /** Display processing execution time */
  data->execution_time_ms = exectime_showtime (data->time_start) ;
  
  /* printf ("Starting points: %d\n", edges_start_points.count) ; */

  printf ("[Edges detection...]\n") ;
  
  /** Explore edges from nodes connex start points, and get edges */
  for (i=0 ; i < edges_start_points.count ; i++) {
    explore_edge_it(matrix, W, H, edges_start_points.point[i].x, edges_start_points.point[i].y, &data->edges) ;
  }
  
  /** Display the number of edges */
  printf ("Edges: %d\n", data->edges.count) ;

  /** Display edges coordinates */
  for (i=0 ; i < data->edges.count ; i++) {
    node1 = data->edges.edge[i].start ;
    node2 = data->edges.edge[i].end ;
    printf ("(%d, %d) - (%d, %d)\n", data->nodes_centers.point[node1].x-1, data->nodes_centers.point[node1].y-1, data->nodes_centers.point[node2].x-1, data->nodes_centers.point[node2].y-1) ;
  }
  
  printf("=== END PROCESSING ===\n");

  /** Display processing execution time */
  data->execution_time_ms = exectime_showtime (data->time_start) ;
  
  /* give_matrix_write (data->matrix, w, h, "matrix4.txt"); */
  
  /** Free memory */
  /* free (matrix) ; */
    
  /* g_object_unref (G_OBJECT (gdk_pixbuf)); */
}


void explore_node_it(int *m, int W, int H, int x, int y, t_points *nodes_centers, t_points *edges_start_points) {
  /* t_pointss unexplored ; */
  int *px, *py ;
  int i, j, nxs, nys ;
  int xmin=x, ymin=y, xmax=x, ymax=y ;
  int cur_node = nodes_centers->count ;
    
  *(m + y*W + x) = cur_node ; /* C_NODE_SEEN */

  unexplored.point[0].x = x ;
  unexplored.point[0].y = y ;
  unexplored.count = 1 ;

  while (unexplored.count > 0) {
    unexplored.count-- ;
    nxs = unexplored.point[unexplored.count].x + 1 ;
    nys = unexplored.point[unexplored.count].y + 1 ;
    
    py = m + (nys-2) * W ;
    
    for (j=nys-2 ; j<=nys ; j++) {
      for (i=nxs-2 ; i<=nxs ; i++) {
        px = py + i ;
        if (*px == C_NODE) {
          if (i < xmin)
            xmin = i ;
          if (j < ymin)
            ymin = j ;

          if (i > xmax)
            xmax = i ;
          if (j > ymax)
            ymax = j ;
          
          *px = cur_node ; /* C_NODE_SEEN */

          unexplored.point[unexplored.count].x = i ;
          unexplored.point[unexplored.count].y = j ;
          unexplored.count++ ;
        }
        else if (*px == C_EDGE) {
          edges_start_points->point[edges_start_points->count].x = i ;
          edges_start_points->point[edges_start_points->count].y = j ;
          edges_start_points->count++;
        }
        
      }
      py += W ;
    }
  }
  
  nodes_centers->point[nodes_centers->count].x = xmin + (xmax - xmin) * .5 ;
  nodes_centers->point[nodes_centers->count].y = ymin + (ymax - ymin) * .5 ;
  nodes_centers->count++ ;
}

void add_node(t_integers *connex_nodes, int val) {
  int i ;
  char found = 0 ;
  
  /* printf ("add connex node %d\n", val) ; */
  for (i=0 ; i < connex_nodes->count && !found; i++) {
    if (connex_nodes->value[i] == val) {
      found = 1 ;
    }
  }
  if (!found) {
    connex_nodes->value[connex_nodes->count++] = val ;
  }
}

void add_edge(t_edges *edges, int node1, int node2) {
  int i, n1, n2 ;
  char found = 0 ;

  if (node1 < node2) {
    n1 = node1 ;
    n2 = node2 ;
  }
  else {
    n1 = node2 ;
    n2 = node1 ;
  }

  for (i=0 ; i < edges->count && !found ; i++) {
    if (edges->edge[i].start == n1 && edges->edge[i].end == n2) {
      found = 1 ;
    }
  }
  
  if (!found) {
    edges->edge[edges->count].start = n1 ;
    edges->edge[edges->count].end = n2 ;
    edges->count++ ;
  }
}

void explore_edge_it(int *m, int W, int H, int x, int y, t_edges *edges) {
  /* t_pointss unexplored ; */
  int *px, *py ;
  t_integers connex_nodes ;
  int i, j, nxs, nys ;

  connex_nodes.count=0 ;

  unexplored.point[0].x = x ;
  unexplored.point[0].y = y ;
  unexplored.count = 1 ;

  while (unexplored.count > 0) {
    unexplored.count-- ;
    nxs = unexplored.point[unexplored.count].x + 1 ;
    nys = unexplored.point[unexplored.count].y + 1 ;
    
    py = m + (nys-2) * W ;
    
    for (j=nys-2 ; j<=nys ; j++) {
      for (i=nxs-2 ; i<=nxs ; i++) {
        px = py + i ;
        if (*px >= 0) {
          add_node(&connex_nodes, *px) ;
        }
        else if (*px == C_EDGE) {
          *px = C_EDGE_SEEN ;
          
          unexplored.point[unexplored.count].x = i ;
          unexplored.point[unexplored.count].y = j ;
          unexplored.count++ ;
        }
      }
      py += W ;
    }
  }
  
  /* printf ("Connex nodes: %d\n", cn) ; */
  
  /** Create edges from connex nodes */
  for (i=0 ; i < connex_nodes.count ; i++) {
    for (j=i+1 ; j < connex_nodes.count ; j++) {
        add_edge(edges, connex_nodes.value[i], connex_nodes.value[j]);
    }
  }
  
}

void give_matrix_write (const char *filename, t_image data) {
  FILE *desc = fopen (filename, "w") ;
  int *px, *py ;
  int x, y ;

  printf ("Writting matrix file \"%s\".\n", filename) ;
  
  if (desc == NULL) {
    perror ("Error: cannot open file in write mode") ;
    exit (1) ;
  }

  py = data.matrix + data.w + 2 ;
  for (y=1 ; y<=data.h ; y++) {
    for (x=1 ; x<=data.w ; x++) {
      px = py + x ;
      if (*px == C_EMPTY) {
        fprintf (desc, "   ") ;
      }
      else {
        fprintf (desc, "%2d ", *px) ;
      }
    }
    py += data.w + 2 ;
    fprintf (desc, "\n") ;
  }

  fclose (desc) ;
}

void give_log_write (const char *logfilename, t_image data) {
  FILE *desc = fopen (logfilename, "a+") ;
  int i ;

  printf ("Writting log file \"%s\".\n", data.filename) ;
  
  if (desc == NULL) {
    perror ("Error: cannot open file in write mode") ;
    exit (1) ;
  }

  fprintf (desc, "%s;%f;%d;%d\n", data.filename, data.execution_time_ms, data.nodes_centers.count, data.edges.count) ;

  /** Nodes coordinates */
  for (i=0 ; i < data.nodes_centers.count ; i++) {
    if (i>0) {
      fprintf (desc, ";") ;
    }
    fprintf (desc, "%d|%d", data.nodes_centers.point[i].x-1, data.nodes_centers.point[i].y-1) ;
  }
  fprintf (desc, "\n") ;

  /** Edges coordinates */
  for (i=0 ; i < data.edges.count ; i++) {
    if (i>0) {
      fprintf (desc, ";") ;
    }
    fprintf (desc, "%d|%d", data.edges.edge[i].start, data.edges.edge[i].end) ;
  }
  fprintf (desc, "\n") ;

  fclose (desc) ;
}

void set_pixel_color_from_matrix (guchar *pr, guchar *pg, guchar *pb, int val) {
  if (val == C_EMPTY) { /* WHITE */
    *pr = 255 ;
    *pg = 255 ;
    *pb = 255 ;
  }
  else if (val == C_NODE) { /* GREEN */
    *pr = 0 ;
    *pg = 255 ;
    *pb = 0 ;
  }
  else if (val >=0) { /* NODE SEEN => LIGHT GREEN */
    *pr = 0 ;
    *pg = 128 ;
    *pb = 0 ;
  }
  else if (val == C_EDGE) { /* UNREACHED EDGE PIXELS => WHITE */
    *pr = 255 ;
    *pg = 255 ;
    *pb = 255 ;
  }
  else if (val == C_EDGE_SEEN) { /* LIGHT RED */
    *pr = 128 ;
    *pg = 0 ;
    *pb = 0 ;
  }
  else { /* BLACK */
    *pr = 0 ;
    *pg = 0 ;
    *pb = 0 ;
  }
}

void draw_pixbuf_from_matrix (GdkPixbuf *gdk_pixbuf, t_image data) {
  int *matrix, *px, *py ;
  guchar *pr, *pg, *pb ;
  guchar *buffer ;
  int x, y, w, h, W ;
  
  w = data.w ;
  h = data.h ;
  matrix = data.matrix ;
  W = data.W ;
  
  buffer = gdk_pixbuf_get_pixels(gdk_pixbuf) ;
  
  if (gdk_pixbuf_get_has_alpha(gdk_pixbuf)) {
    /* printf ("RGBA\n") ; */
    
    py = matrix + W ;
    for (y=1 ; y<=h ; y++) {
      for (x=1 ; x<=w ; x++) {
        px = py + x ;

        pr = buffer++ ;
        pg = buffer++ ;
        pb = buffer++ ;
        buffer++ ;

        set_pixel_color_from_matrix (pr, pg, pb, *px) ;
      }
      
      py += W ;
    }
  }
  else {
    /* printf ("RGB\n") ; */

    py = matrix + W ;
    for (y=1 ; y<=h ; y++) {
      for (x=1 ; x<=w ; x++) {
        px = py + x ;

        pr = buffer++ ;
        pg = buffer++ ;
        pb = buffer++ ;

        set_pixel_color_from_matrix (pr, pg, pb, *px) ;
      }
      
      py += W ;
    }
  }

}
