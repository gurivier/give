/**
 * Functions allowing to access and modify the pixels of a GdkPixbuf.
 *
 * AUTHOR: Guillaume Riviere (C) 2012
 * FILE: gdkpixbuf_accessors.c
 */

#include "gdkpixbuf_accessors.h"

gboolean gdkpixbuf_get_pixel_color (GdkPixbuf *pixbuf, gint x, gint y, guchar *red, guchar *green, guchar *blue) {
  guchar *buffer = gdk_pixbuf_get_pixels(pixbuf) ;
  gint row_size = gdk_pixbuf_get_rowstride (pixbuf) ;
  gint pos = gdk_pixbuf_get_has_alpha (pixbuf) ? (y * row_size ) + (x * 4) : (y * row_size) + (x * 3) ;

  *red   = buffer[pos] ;
  *green = buffer[pos + 1] ;
  *blue  = buffer[pos + 2] ;
 
  return TRUE ;
}

gboolean gdkpixbuf_set_pixel_color (GdkPixbuf *pixbuf, gint x, gint y, guchar red, guchar green, guchar blue) {
  guchar *buffer = gdk_pixbuf_get_pixels(pixbuf) ;
  gint row_size = gdk_pixbuf_get_rowstride (pixbuf) ;
  gint pos = gdk_pixbuf_get_has_alpha (pixbuf) ? (y * row_size ) + (x * 4) : (y * row_size) + (x * 3) ;

  buffer[pos] = red ;
  buffer[pos + 1] = green ;
  buffer[pos + 2] = blue ;
 
  return TRUE ;
}
