/**
 * This module retrieves a graph from an image and gives
 * vertices' coordinates and edges topology. The color of
 * the image background, the vertices, and the edges must
 * fit in ranges of values.
 *
 * AUTHOR: Guillaume Riviere (C) 2021
 * FILE: main.c
 * VERSION: 0.07
 */

#ifndef __GIVE_H
#define __GIVE_H

#include "exectime.h"

#define C_EMPTY     -9
#define C_NODE      -5
#define C_EDGE      -7
#define C_EDGE_SEEN -8

#define MAX_SIZE 1024

typedef struct point {
  int x ;
  int y ;
} t_point ;

typedef struct points {
  t_point point[MAX_SIZE] ;
  int count ;
} t_points ;

typedef struct pointss {
  t_point point[MAX_SIZE*10] ;
  int count ;
} t_pointss ;

typedef struct edge {
  int start ;
  int end ;
} t_edge ;

typedef struct edges {
  t_edge edge[MAX_SIZE] ;
  int count ;
} t_edges ;

typedef struct integers {
  int value[MAX_SIZE] ;
  int count ;
} t_integers ;

typedef struct image {
  char *filename ;
  /* Image pixels */
  GdkPixbuf *gdk_pixbuf ;
  int w, h ;
  /* Image extended matrix */
  int *matrix ;
  int W, H ;
  /* Coordinates */
  t_points nodes_centers ;
  t_edges edges ;
  /* Execution time */
  t_exectime time_start ;
  double execution_time_ms ;
} t_image ;

typedef struct params {
  unsigned char
    node_rmin, node_rmax,
    node_gmin, node_gmax,
    node_bmin, node_bmax,
    edge_rmin, edge_rmax,
    edge_gmin, edge_gmax,
    edge_bmin, edge_bmax ;
} t_params ;

t_image give_open_image (char *filename) ;
void give_free_image (t_image data) ;
void give_extract (t_image *data, t_params params) ;

void give_matrix_write (const char *filename, t_image data) ;
void give_log_write (const char *logfilename, t_image data) ;

void draw_pixbuf_from_matrix (GdkPixbuf *gdk_pixbuf, t_image data) ;

#endif /* __GIVE_H */
