/**
 * Functions allowing to access and modify the pixels of a GdkPixbuf.
 *
 * AUTHOR: Guillaume Riviere (C) 2012
 * FILE: gdkpixbuf_accessors.h
 */

#ifndef GDKPIXBUF_ACCESSORS_H
#define GDKPIXBUF_ACCESSORS_H

#include <gtk/gtk.h>

/**
 * This function gets a pixel's components from a GdkPixbuf.
 *
 * @param GdkPixbuf : the structure to access
 * @param x         : the x coordinate of the pixel
 * @param y         : the y coordinate of the pixel
 * @param red       : the red component of the pixel (in range 0 to 255)
 * @param green     : the green component of the pixel (in range 0 to 255)
 * @param blue      : the blue component of the pixel (in range 0 to 255)
 * @return Will returns TRUE if successful, or FALSE if not
 */
gboolean gdkpixbuf_get_pixel_color (GdkPixbuf *pixbuf, gint x, gint y, guchar *red, guchar *green, guchar *blue) ;

/**
 * This function sets a pixel's components in a GdkPixbuf.
 *
 * @param GdkPixbuf : the structure to access
 * @param x         : the x coordinate of the pixel
 * @param y         : the y coordinate of the pixel
 * @param red       : the new red component for the pixel (in range 0 to 255)
 * @param green     : the new green component for the pixel (in range 0 to 255)
 * @param blue      : the new blue component for the pixel (in range 0 to 255)
 * @return Will returns TRUE if successful, or FALSE if not
 */
gboolean gdkpixbuf_set_pixel_color (GdkPixbuf *pixbuf, gint x, gint y, guchar red, guchar green, guchar blue) ;


#endif /* GDKPIXBUF_ACCESSORS_H */
