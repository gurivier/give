/**
 * This module helps measuring and showing time.
 *
 * AUTHOR: Guillaume Riviere (C) 2021
 * FILE: main.c
 * VERSION: 0.07
 */

#include <stdio.h>

#include "exectime.h"

t_exectime exectime_gettime () {
  t_exectime cur_time ;

#ifdef __unix
  clock_gettime(CLOCK_MONOTONIC_RAW, &cur_time);
#elif _WIN32
  GetSystemTime(&cur_time);
#endif

  return cur_time ;
}

double exectime_showtime (t_exectime time_start) {
  double execution_time_ms ;
  t_exectime time_end ;
  
#ifdef __unix
  
  unsigned long int execution_time_us ;
  
  clock_gettime(CLOCK_MONOTONIC_RAW, &time_end);
  
  execution_time_us = (time_end.tv_sec - time_start.tv_sec) * 1000000 + (time_end.tv_nsec - time_start.tv_nsec) / 1000;
  execution_time_ms = execution_time_us/1000.0 ;
  printf (">Execution time: %.1f ms (%lu us)\n", execution_time_ms, execution_time_us) ;

#elif _WIN32
  
  LONG time_start_ms, time_end_ms;
  
  GetSystemTime(&time_end);
  
  time_start_ms = (time_start.wSecond * 1000) + time_start.wMilliseconds;
  time_end_ms = (time_end.wSecond * 1000) + time_end.wMilliseconds;
  execution_time_ms = time_end_ms - time_start_ms ;
  printf (">Execution time: %.1f ms\n", execution_time_ms) ;

#endif

  return execution_time_ms ;
}
