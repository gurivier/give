#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <gtk/gtk.h>

G_MODULE_EXPORT void on_mainwindow_destroy (GtkObject *object, gpointer user_data) ;

G_MODULE_EXPORT void on_viewport_original_motion_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) ;
G_MODULE_EXPORT void on_viewport_treated_motion_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) ;

G_MODULE_EXPORT void on_viewport_original_leave_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) ;
G_MODULE_EXPORT void on_viewport_treated_leave_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) ;

#endif /* CALLBACKS_H */
