#include <stdio.h>   /* sprintf */
#include <stdlib.h>  /* atoi */
#include <string.h>  /* strlen, strcmp */
#include <ctype.h>   /* isdigit */

#include "callbacks.h"


extern GtkBuilder *builder;

G_MODULE_EXPORT void on_mainwindow_destroy (GtkObject *object, gpointer user_data) {
  printf("Closing main window.\n");
  gtk_main_quit();
}

G_MODULE_EXPORT void on_viewport_original_motion_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) {
 
  if (event->type==GDK_MOTION_NOTIFY) {
    GtkStatusbar *statusbar1 = GTK_STATUSBAR (gtk_builder_get_object (builder, "statusbar1"));
    GdkEventMotion* e=(GdkEventMotion*)event;
    gchar *buff;

    buff = g_strdup_printf ("(%u ; %u)", (guint)e->x,(guint)e->y);
    gtk_statusbar_push (GTK_STATUSBAR (statusbar1), 0, buff);
    g_free (buff);
  }
  
}

G_MODULE_EXPORT void on_viewport_treated_motion_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) {
 
  if (event->type==GDK_MOTION_NOTIFY) {
    GtkStatusbar *statusbar1 = GTK_STATUSBAR (gtk_builder_get_object (builder, "statusbar1"));
    GdkEventMotion* e=(GdkEventMotion*)event;
    gchar *buff;

    buff = g_strdup_printf ("(%u ; %u)", (guint)e->x,(guint)e->y);
    gtk_statusbar_push (GTK_STATUSBAR (statusbar1), 0, buff);
    g_free (buff);
  }
  
}

G_MODULE_EXPORT void on_viewport_original_leave_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) {
  GtkStatusbar *statusbar1 = GTK_STATUSBAR (gtk_builder_get_object (builder, "statusbar1"));
  gtk_statusbar_push (GTK_STATUSBAR (statusbar1), 0, "(;)");
}

G_MODULE_EXPORT void on_viewport_treated_leave_notify_event (GtkObject *object, GdkEvent *event, gpointer user_data) {
  GtkStatusbar *statusbar1 = GTK_STATUSBAR (gtk_builder_get_object (builder, "statusbar1"));
  gtk_statusbar_push (GTK_STATUSBAR (statusbar1), 0, "(;)");
}
